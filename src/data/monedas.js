const monedas = [
    {id: 'USD', nombre: 'Dolar estaodunidense'},
    {id: 'MXN', nombre: 'Peso mexicano'},
    {id: 'EUR', nombre: 'Euro'},
    {id: 'GBP', nombre: 'Libra esterlina'}
]

export {
    monedas
}